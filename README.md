# gentoo\_stuff\_t430
The files contained in this repo are used in my personal gentoo system.  
**Use with portage profile default/linux/amd64/17.0/desktop**

## Files
**kernel-config**  
Config for a Lenovo Thinkpad T430 supporting:  

 * (only) integrated graphics
 * Intel wifi/bluetooth adapter **(sys-kernel/linux-firmware is needed)**
 * NAT/Bridging Stuff for vagrant
 * all t430 features
 * **NO SELinux** because meh.
 * filesystems
  * ext2-4
  * nfs
  * fat
  * ntfs
  * zfs **(sys-fs/zfs and dependencies are required)**
 * Support for encrypted rootfs.
 * Support for kvm
 * **No support for systemd!!!**

**make.conf**  
Make.conf to build a system with:   

 * support for all t430 features
 * no systemd
 * support for intel i965 graphics
 * global vim-syntax
 * some other stuff I use.
 * bindist bs disabled
